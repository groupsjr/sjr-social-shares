<?php
/*
Plugin Name:	SJR Social Media Shares
Plugin URI:		
Description:	Shows number of shares from social networks
Author:			Group SJR | Eric Eaglstun
Text Domain:	sjr-social-shares
Domain Path:	/lang
Version:		0.7.2
Author URI:		
*/

// require php 5.3
register_activation_hook( __FILE__, create_function("", '$ver = "5.3"; if( version_compare(phpversion(), $ver, "<") ) die( "This plugin requires PHP version $ver or greater be installed." );') );

// @TODO check for sjr core with \sjr\version

// load it
require __DIR__.'/index.php';