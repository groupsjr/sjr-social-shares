<?php

namespace sjr\social_shares;

define( 'SJR_SOCIAL_SHARES_VERSION', '0.7.2' );

require __DIR__.'/functions.php';
require __DIR__.'/lib/defaults.php';

/**
*	enqueue font awesome on front end and admin
*	attached to `admin_enqueue_scripts` action
*	attached to `wp_enqueue_scripts` action
*/
function register_scripts_font_awesome(){
	wp_register_style( 'font-awesome', '//netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', array(), '', 'all' );
	wp_enqueue_style( 'font-awesome' );
}
add_action( 'admin_enqueue_scripts', __NAMESPACE__.'\register_scripts_font_awesome', 10 );
add_action( 'wp_enqueue_scripts', __NAMESPACE__.'\register_scripts_font_awesome', 10 );

/**
*	callback for [shares] shortcode
*	@param array
*		echo - bool echo or return html
*		items - space separated social networks
*		post_id - int optional
*		show - string items/total
*	@return string
*/
function shortcode( $atts ){
	$atts = wp_parse_args( $atts, array(
		'echo' => TRUE,
		'items' => 'twitter facebook linkedin email comments',
		'post_id' => get_the_ID(),
		'show' => 'total'
	) );
	
	$atts['items'] = array_map( 'trim', explode(' ', $atts['items']) );
	
	switch( $atts['show'] ){
		case 'total':
			$vars = array(
				'total' => get_shares_total( $atts['post_id'], $atts['items'] )
			); 
			
			$template = 'total';
			break;
			
		case 'items':
		default:
			$vars = array(
				'items' => get_shares_items( $atts['post_id'], $atts['items'] )
			);
			
			$template = 'items';
			break;
	}
	
	$template_file = apply_filters( 'sjr\social_shares\template', 'social/'.$template.'.php' );
	
	$vars = array_merge( $atts, $vars );
	
	if( $template_file = locate_template($template_file) )
		$html = render( $template_file, $vars );
	else
		$html = render( $template, $vars );
	
	if( $atts['echo'] )
		echo $html;
	else
		return $html;
}
add_shortcode( 'shares', __NAMESPACE__.'\shortcode' );