<?php

namespace sjr\social_shares;

/**
*	gets number of comments on post
*	@param int
*	@return int
*/
function get_comment_count( $post_id ){
	return wp_count_comments( $post_id )->approved;
}

/**
*	gets count of urls shared on email
*	@TODO figure out how this is supposed to work
*	@param int
*	@return int
*/
function get_email_count( $post_id ){
	//return rand( 0, 10 );
	return '';
}

/**
*	gets count of share url on facebook
*	@param int
*	@return int
*/
function get_facebook_count( $post_id ){
	$url = $url = get_permalink( $post_id );
	$response = wp_remote_get( 'http://graph.facebook.com/?id='.urlencode($url) );
	$body = wp_remote_retrieve_body( $response ); 
	
	$body = json_decode( $body );
	
	return isset( $body->shares ) ? (int) $body->shares : 0;
}

/**
*	gets share count of url shared google plus
*	@param int
*	@return int
*/
function get_google_count( $post_id ){
	$url = get_permalink( $post_id );
	$response = wp_remote_get( 'https://plusone.google.com/_/+1/fastbutton?url='.urlencode($url), array(
		'decompress' => FALSE
	) );
	$body = wp_remote_retrieve_body( $response ); 
	
	if( trim($body) ){
	    $doc = new \DOMDocument;
	    libxml_use_internal_errors( TRUE );
	    $doc->loadHTML( $body );
	    $counter = $doc->getElementById( 'aggregateCount' );
	    
	    return (int) $counter->nodeValue;
    } else {
    	return 0;
    }
}

/**
*	gets share count of url shared on linkedin
*	does not need http/https or www/non www versions of url
*	@param int
*	@return int
*/
function get_linkedin_count( $post_id ){
	$url = get_permalink( $post_id );
	$response = wp_remote_get( 'http://www.linkedin.com/countserv/count/share?url='.urlencode($url), array(
		'decompress' => FALSE
	) );
	$body = wp_remote_retrieve_body( $response ); 
	
	if( trim($body) ){
		// comes back as data within a js callback, so get string between parentheses
		preg_match( '/\((.*?)\)/', $body, $matches );
		$data = json_decode( $matches[1] );
		
		return (int) $data->count;
	}
	
	return 0;
}

/**
*	gets count of url shared on pinterest
*	@param int
*	@return int
*/
function get_pinterest_count( $post_id ){
	$url = get_permalink( $post_id );
	$response = wp_remote_get( 'http://api.pinterest.com/v1/urls/count.json?url='.urlencode($url), array(
		'decompress' => FALSE
	) );
	$body = wp_remote_retrieve_body( $response );
	
	if( trim($body) ){
		// comes back as data within a js callback, so get string between parentheses
		preg_match( '/\((.*?)\)/', $body, $matches );
		$data = json_decode( $matches[1] );
		
		return (int) $data->count;
	}
	
	return 0;
}

/**
*
*	@param int
*	@return int
*/
function get_tumblr_count( $post_id ){

}

/**
*	gets count of url shared on twitter
*	@TODO fix, cdn.api.twitter.com is shutdown
*	@param int
*	@return int
*/
function get_twitter_count( $post_id ){
	$url = get_permalink( $post_id );
	$response = wp_remote_get( 'https://cdn.api.twitter.com/1/urls/count.json?url='.urlencode($url), array(
		'decompress' => FALSE
	) );
	$body = wp_remote_retrieve_body( $response ); 
	$body = json_decode( $body );
	
	return isset($body->count) ? (int) $body->count : 0;
}