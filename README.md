# SJR Social Shares

## Services

Default servies are Comments, Email, Facebook, Google Plus, LinkedIn, Pinterest, and Twitter.  These can be customized using  `sjr\social_shares\callbacks`, `sjr\social_shares\icons`, `sjr\social_shares\site`, and `sjr\social_shares\title` filters

## Shortcode

The widget can be inserted directly into a post using the `[shares]` shortcode or called with the `do_shortcode` function with the following attributes

`echo` - 0 to return the html, 1 to echo directly.  Default 1

`items` - space spearated list of servies to show.  Array keys from `get_share_callback` etc. Default `twitter facebook linkedin email comments`

`post_id` - ID of post to show, blank uses value from `get_the_ID` 

`show` - `items` or `total` Show individual social services or aggregated total of share counts

Example

`[shares echo='1' items='twitter facebook linkedin' post_id='23' show='items']`

## Markup 

Default templates are in the `views` directory

If `social/items.php` or `social/total.php` exist in the theme's directory these will be used instead with `$items` `$total` variables available