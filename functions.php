<?php

namespace sjr\social_shares;

/**
*	the services and callback for sharing services
*	uses `sjr\social_shares\callbacks` filter for customizable callbacks
*	@param string
*	@param int
*	@return int
*/
function get_share_callback( $item, $post_id ){
	static $callbacks = NULL;
	if( is_null($callbacks) ){
		$callbacks = apply_filters( 'sjr\social_shares\callbacks', array(
			'comments' => __NAMESPACE__.'\get_comment_count',
			'email' => __NAMESPACE__.'\get_email_count',
			'facebook' => __NAMESPACE__.'\get_facebook_count',
			'googleplus' => __NAMESPACE__.'\get_google_count',
			'linkedin' => __NAMESPACE__.'\get_linkedin_count',
			'pinterest' => __NAMESPACE__.'\get_pinterest_count',
			'tumblr' => __NAMESPACE__.'\get_tumblr_count',
			'twitter' => __NAMESPACE__.'\get_twitter_count'
		) );
	}
	
	if( is_callable($callbacks[$item]) )
		return call_user_func( $callbacks[$item], $post_id );
	
	// this is just for dummy data if callback doesn't exist
	return 'x';
}

/**
*	defaults use font awesome classes
*	uses `sjr\social_shares\icons` filter
*	@param string
*	@return string
*/
function get_share_icon( $item ){
	static $icons = NULL;
	if( is_null($icons) ){
		$icons = apply_filters( 'sjr\social_shares\icons', array(
			'comments' => 'comment',
			'email' => 'envelope',
			'facebook' => 'facebook',
			'googleplus' => 'google-plus-square',
			'linkedin' => 'linkedin',
			'pinterest' => 'pinterest',
			'twitter' => 'twitter'
		) );
	}
	
	return isset( $icons[$item] ) ? $icons[$item] : $item;
}

/**
*	items usually used as addthis service code
*	@see https://www.addthis.com/services
*	@uses `sjr\social_shares\site` filter 
*	@param string
*	@return string
*/
function get_share_site( $item ){
	static $sites = NULL;
	if( is_null($sites) ){
		$sites = apply_filters( 'sjr\social_shares\site', array(
			'comments' => '',
			'email' => 'email',
			'facebook' => 'facebook',
			'googleplus' => 'google_plusone_share',
			'linkedin' => 'linkedin',
			'pinterest' => 'pinterest_share',
			'twitter' => 'twitter'
		) );
	}
	
	return isset( $sites[$item] ) ? $sites[$item] : '';
}

/**
*	
*	@uses `sjr\social_shares\title` filter
*	@param string
*	@return string
*/
function get_share_title( $item ){
	static $titles = NULL;
	if( is_null($titles) ){
		$titles = apply_filters( 'sjr\social_shares\title', array(
			'comments' => 'Comment',
			'email' => 'Email',
			'facebook' => 'Share',
			'googleplus' => 'Share',
			'linkedin' => 'Share',
			'pinterest' => 'Pin It',
			'twitter' => 'Tweet'
		) );
	}
	
	return isset( $titles[$item] ) ? $titles[$item] : '';
}

/**
*	get share info and other data for shares on social networks
*	@uses `sjr\social_shares\items` filter
*	@param int
*	@param array
*	@return array
*/
function get_shares_items( $post_id, $items ){
	$transient_key = transient_key( 'sjr-shares', array($post_id, $items) );
	
	if( !($result = get_transient($transient_key)) ){
		$values = array_map( function($item) use($post_id){ 
			return (object) array(
				'icon' => get_share_icon( $item ),
				'post_id' => $post_id,
				'shares' => get_share_callback( $item, $post_id ),
				'site' => get_share_site( $item ),
				'title' => get_share_title( $item )
			);
		}, $items );
		
		// keys are classes for font awesome
		$result = array_combine( $items, $values );
		$result = apply_filters( 'sjr\social_shares\items', $result );
		
		set_transient( $transient_key, $result, 300 );
	}
	
	return $result;
}

/**
*	gets the total number of shares for the services queried
*	@param int
*	@param array
*	@return int
*/
function get_shares_total( $post_id, $items ){
	$shares = get_shares_items( $post_id, $items );
	
	$sum = array_sum( array_map(function($item){
		return is_int( $item->shares ) ? $item->shares : 0;
	}, $shares) );
	
	return $sum;
}

/**
*	gets www and non www versions of a url, no scheme
*	@param string
*	@return array
*/
function get_share_urls( $url ){
	$parts = parse_url( $url );
	
	$original = $parts['host'].$parts['path'];
	
	$return = array(
		$original,
		strpos( $original, 'www.' ) === 0 ? substr( $original, 4 ) : 'www.'.$original
	);
	
	return $return;
}

/**
*	render a page into wherever
*	@param string filename inside /views/ directory, no trailing .php
*	@param object|array variables available to view
*	@return string html
*/
function render( $_template, $vars = array() ){
	if( substr($_template, -4) == '.php' )
		$_template_file = $_template;
	elseif( file_exists(__DIR__.'/views/'.$_template.'.php') )
		$_template_file = __DIR__.'/views/'.$_template.'.php';
	else
		return "<div>template missing: $_template</div>";
		
	extract( (array) $vars, EXTR_SKIP );
	
	ob_start();
	require $_template_file;
	$html = ob_get_contents();
	ob_end_clean();
	
	return $html;
}

/**
*
*	@param string
*	@param array
*	@return string
*/
function transient_key( $prefix, array $vars ){
	$vars[] = version();

	$key = substr( $prefix.'-'.md5(serialize($vars)), 0, 40 );

	return $key;
}

/**
*	gets current version of plugin
*	@return string
*/
function version(){
	static $version;

	if( !$version ){
		$data = get_file_data( __DIR__.'/_plugin.php', array('Version'), 'plugin' );
		$version = $data[0];
	}
	
	return $version;
}